package controllers

import play.api.mvc._
import javax.inject._

import org.webjars.play.RequireJS

class Application  @Inject()(webJarAssets: WebJarAssets, requireJS: RequireJS) extends Controller {

  def index = Action {
    Ok(views.html.index(webJarAssets, requireJS))
  }
}

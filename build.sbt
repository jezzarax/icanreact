name := """iCanReact"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala, SbtWeb)


scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "com.github.nscala-time" 	%% 	"nscala-time" 					% "2.12.0",
  "org.scalikejdbc" 		%% 	"scalikejdbc" 					% "2.4.2",
  "ch.qos.logback"  		%  	"logback-classic"   			% "1.0.13",
  "mysql" 					% 	"mysql-connector-java" 			% "5.1.39",

  "org.webjars" 			%% 	"webjars-play" 					% "2.5.0",
  "org.webjars" 			% 	"bootstrap" 					% "3.1.1-2",
  "org.webjars.bower" 		% 	"babel" 						% "5.8.38",
  "org.webjars.bower" 		% 	"react" 						% "15.3.1",
  "org.webjars.bower" 		% 	"commonjs-require" 				% "1.4.5",
  "org.webjars.npm" 		% 	"fetch-polyfill"				% "0.8.1",

  "org.scalatestplus" 		% 	"play_2.11" 					% "1.4.0" % Test,
  "org.mockito" 			% 	"mockito-core" 					% "1.9.5" % Test,
  "org.scalamock" 			%% 	"scalamock-scalatest-support" 	% "3.2.2" % Test

)
